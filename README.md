# Mine Control Decision Support

Mine Control Decision Support is a tool to support mine controllers in making decisions based on...

## Project Layout

The project is split into two subprojects:

- _Web interface_: a Single-Page Application in [React.js](https://facebook.github.io/react/) showing ... 
- _Services_: the back-end services serving the Web Interface

For more information, consult the documentation on each subproject:

- [_Web interface_](Web interface/README.md)
- [_Services_](Services/README.md)

