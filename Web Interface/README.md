# Web Interface

This project is the web interface for the Mine Control Decision Support. It is a [React.js](https://facebook.github.io/react/) application based on the [React Static Boilerplate](https://github.com/kriasoft/react-static-boilerplate) (version [4a33054](https://github.com/kriasoft/react-static-boilerplate/commit/4a33054)).

## Technologies

The project uses [ES6](http://es6-features.org/) and the following stack:

- [React.js](http://facebook.github.io/react/)
- [React Hot Loader](http://gaearon.github.io/react-hot-loader/)
- [Redux](http://redux.js.org/)
- [Babel](http://babeljs.io/)
- [Webpack](https://webpack.github.io/)
- [Jest](https://facebook.github.io/jest/)
- [PostCSS](https://github.com/postcss/postcss)

For more information on React.js, check the following documentation: https://github.com/kriasoft/react-static-boilerplate#learn-reactjs-and-es6

## Building and running the tests

To build the application:

    npm install

To run the tests:

    npm test

To start the application in Development mode (at http://localhost:3000):

    npm start

## Directory layout

The layout of the project is derived from the [React Static Boilerplate](https://github.com/kriasoft/react-static-boilerplate):

```
├── components/                 # Shared or generic UI components
│   └── ...                     
├── docs/                       # Documentation to the project
├── node_modules/               # 3rd-party libraries and utilities
├── src/                        # Application source code
│   ├── error/                  # Error page
│   ├── home/                   # Home page
│   ├── history.js              # Handles client-side navigation
│   ├── main.js                 # <== Application entry point <===
│   ├── router.js               # Handles routing and data fetching
│   ├── routes.json             # This list of application routes
│   └── store.js                # Application state manager (Redux)
├── public/                     # Static files such as favicon.ico etc.
│   ├── dist/                   # The folder for compiled output
│   ├── favicon.ico             # Application icon to be displayed in bookmarks
│   └── ...                     # etc.
├── test/                       # Unit and integration tests
├── tools/                      # Utility and helper classes
└── package.json                # The list of project dependencies and NPM scripts
```

## Third-party licenses

This project uses code from the [React Static Boilerplate](https://github.com/kriasoft/react-static-boilerplate) which is licensed under the [MIT License](https://opensource.org/licenses/MIT).