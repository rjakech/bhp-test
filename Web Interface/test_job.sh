#!/bin/bash
USAGE="Usage: command"

echo "Starting npm test:coverage..."
npm run test:coverage
echo "Finished npm test:coverage."

echo "Starting npm test:report..."
npm run test:report
echo "Finished npm test:report."
