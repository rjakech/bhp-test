import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import s from './Timeline.css';
import _ from 'lodash';

class Timeline extends React.Component {

  static propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
  };

  //let parseDate = d3.timeParse("%b %Y");

  buildGraph() {
    var svg = d3.select("svg"),
      margin = { top: 20, right: 20, bottom: 110, left: 40 },
      margin2 = { top: 430, right: 20, bottom: 30, left: 40 },
      width = +svg.attr("width") - margin.left - margin.right,
      height = +svg.attr("height") - margin.top - margin.bottom,
      height2 = +svg.attr("height") - margin2.top - margin2.bottom;

    var x = d3.scaleTime().range([0, width]),
      x2 = d3.scaleTime().range([0, width]),
      y = d3.scaleLinear().range([height, 0]),
      y2 = d3.scaleLinear().range([height2, 0]);


    var color = d3.scaleOrdinal()
      .domain(["equip1", "equip2"])
      .range(["#FF0000", "#009933"]);

    var xAxis = d3.axisBottom(x),
      xAxis2 = d3.axisBottom(x2),
      yAxis = d3.axisLeft(y);


    var line = d3.line()
      //.interpolate("basis")
      .x(function (d) { return x(d.date); })
      .y(function (d) { return y(d.price); });

    var brush = d3.brushX()
      .extent([[0, 0], [width, height2]])
      .on("brush end", () => {
        if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
        var s = d3.event.selection || x2.range();
        x.domain(s.map(x2.invert, x2));
        focus.select(".area")
          .attr("d", area);
        focus.select(".axis--x").call(xAxis);
        svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
          .scale(width / (s[1] - s[0]))
          .translate(-s[0], 0));
      });

    var zoom = d3.zoom()
      .scaleExtent([1, Infinity])
      .translateExtent([[0, 0], [width, height]])
      .extent([[0, 0], [width, height]])
      .on("zoom", () => {
        if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
        var t = d3.event.transform;
        x.domain(t.rescaleX(x2).domain());
        focus.select(".area").attr("d", area);
        focus.select(".axis--x").call(xAxis);
        context.select(".brush").call(brush.move, x.range().map(t.invertX, t));
      });

    var area = d3.area()
      .curve(d3.curveMonotoneX)
      .x(function (d) { return x(d.date); })
      .y0(height)
      .y1(function (d) { return y(d.price); });

    var area2 = d3.area()
      .curve(d3.curveMonotoneX)
      .x(function (d) { return x2(d.date); })
      .y0(height2)
      .y1(function (d) { return y2(d.price); });

    svg.append("defs").append("clipPath")
      .attr("id", "clip")
      .append("rect")
      .attr("width", width)
      .attr("height", height);

    var focus = svg.append("g")
      .attr("class", "focus")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var context = svg.append("g")
      .attr("class", "context")
      .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

    d3.tsv("data.tsv", function (error, data) {
      if (error) throw error;
      console.log('data', data)
      /* preprocessing */

      /* */
      color.domain(d3.keys(data[0]).filter(function (key) { return key !== "date"; }));

      /*data.forEach(function (d) {
        var parseDate = d3.timeParse("%b %Y")
        d.date = parseDate(d.date);
        d.price = +d.price;
      });*/
      data.forEach(function (d) {
        var parseDate = d3.timeParse("%Y%m%d%H%M")
        d.date = parseDate(d.date);
      });

      var equipment = color.domain().map(function (name) {
        return {
          name: name,
          values: data.map(function (d) {
            return { date: d.date, price: +d[name] };
          })
        };
      });
      console.log('equipment', equipment)

      x.domain(d3.extent(data, function (d) { return d.date; }));
      /*y.domain([0, d3.max(data, function (d) { return d.price; })]);*/

      y.domain([
        d3.min(equipment, function (c) { return d3.min(c.values, function (v) { return v.price; }); }),
        d3.max(equipment, function (c) { return d3.max(c.values, function (v) { return v.price; }); })
      ])
      x2.domain(x.domain());
      y2.domain(y.domain());



      focus.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

      focus.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Temperature (ºF)");

      var city = focus.selectAll(".city")
        .data(equipment)
        .enter().append("g")
        .attr("class", "city")
        .attr("fill", "none")
        .style("clip-path", "url(#clip)");

      city.append("path")
        .attr("class", "line")
        .attr("d", function (d) { return line(d.values); })
        .style("stroke", function (d) { return color(d.name); });

      city.append("text")
        .datum(function (d) { return { name: d.name, value: d.values[d.values.length - 1] }; })
        .attr("transform", function (d) { return "translate(" + x(d.value.date) + "," + y(d.value.price) + ")"; })
        .attr("x", 3)
        .attr("dy", ".35em")
        .text(function (d) { return d.name; });

      /*
      focus.append("path")
        .data(equipment)
        //.datum(data)
        .attr("class", "area")
        .attr("d", area)
        .attr("stroke", function(d) { return color(d.name); })
        .attr("stroke-width", 1)
        .attr("fill", "none")
        .style("clip-path", "url(#clip)");
      focus.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

      focus.append("g")
        .attr("class", "axis axis--y")
        .call(yAxis);

      context.append("path")
        //.data(equipment)
        .datum(data)
        .attr("class", "area")
        .attr("d", area2)
        .attr("stroke", function(d) {return color(d.name); })
        .attr("stroke-width", 1)
        .attr("fill", "none")
        .style("clip-path", "url(#clip)");

//*/
      /*
            context.append("g")
              .attr("class", "axis axis--x")
              .attr("transform", "translate(0," + height2 + ")")
              .call(xAxis2);
      
            context.append("g")
              .attr("class", "brush")
              .call(brush)
              .call(brush.move, x.range());
      
            svg.append("rect")
              .attr("class", "zoom")
              .attr("width", width)
              .attr("height", height)
              .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
              .call(zoom)
              .style("cursor", "move")
              .style("fill", "none")
              .style("pointer-events", "all");
          //*/
      var theDate = new Date();
      console.log('theDate', theDate)
      var nowLine = focus.append("g");
      nowLine.append("line")
        .attr("class", "line")
        .attr('stroke', "rgb(0, 0, 255)")
        .attr("x1", x(theDate))
        .attr("y1", height)
        .attr("x2", x(theDate))
        .attr("y2", 0);

      var format24HourTime = d3.timeFormat("%H:%M");
      var formatDate = d3.timeFormat("%-d/%-m/%Y");
      
      nowLine.append("text")
        .attr("class", "currentTime")
        .attr("text-anchor", "middle")
        .attr("x", x(theDate))
        .attr("y", "-0.5em")
        .text(format24HourTime(theDate) + ' ' + formatDate(theDate));
    });

  }

  //brushed

  //zoomed

  // type
  componentDidMount() {
    this.buildGraph();
  }

  render() {
    return (
      <svg className='timeline' height="500" width="960">
      </svg>
    );
  }
}

export default Timeline;
