import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import s from './Dashboard.css';
import Timeline from '../Timeline';

class Dashboard extends React.Component {

  static propTypes = {
    className: PropTypes.string,
  };

  render() {
    return (
      <div className={cx(s.dashboard, this.props.className)}>
        Dashboard
        <Timeline width={960} height={600}></Timeline>
      </div>
    );
  }
}

export default Dashboard;
