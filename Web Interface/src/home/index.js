import React from 'react';
import Dashboard from '../../components/Dashboard';
import s from './styles.css';

class HomePage extends React.Component {

  static propTypes = {
  };

  componentDidMount() {
  }

  render() {
    return (
      <Dashboard className={s.content} />
    );
  }

}

export default HomePage;
