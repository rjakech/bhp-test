import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import Button from '../components/Button';

test('Button renders a HTML button', () => {
  const link = mount(
    <Button />
  );

  expect(link.find('button')).to.have.lengthOf(1);
});

test('Button renders a HTML link when a target href is specified', () => {
  const link = mount(
    <Button href="www.example.com" />
  );

  const actualLinks = link.find('a');
  expect(actualLinks).to.have.lengthOf(1);
  expect(actualLinks.first().prop('href')).to.equal('www.example.com');
});

