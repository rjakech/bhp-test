import React from 'react';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';
import Link from '../components/Link';

test('Link renders link text', () => {
  const link = shallow(
    <Link to="www.example.com">Example link</Link>
  );

  expect(link.text()).toEqual('Example link');
});

test('Link renders a single link', () => {
  const link = mount(
    <Link to="www.example.com">Example link</Link>
  );

  expect(link.find('a').length).toBe(1);
});

test('Link renders the expected HTML link', () => {
  const link = renderer.create(
    <Link to="www.example.com">Example link</Link>
  );

  const linkContent = link.toJSON();
  expect(linkContent).toMatchSnapshot();
});
